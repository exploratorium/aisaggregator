/*jslint esnext: true */
/*jslint node: true */
"use strict";
var express = require('express');
var path = require('path');
var request = require('request');
//var logger = require('morgan');
var bodyParser = require('body-parser');
var fs = require('fs');
var exploais = require('./exploais/exploais.js');
var aisDecoder = new exploais.textDecoder();
var app = express();
var clientId = 0;
app.engine('html', require('ejs').renderFile);
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var net = require('net');
var streamsJson;
try {
  streamsJson = (JSON.parse(fs.readFileSync('streams.json'))).streams;
} catch (err) {
  var defaults = JSON.parse(fs.readFileSync('defaults.json'));
  streamsJson = defaults.streams;
  fs.writeFile('streams.json', JSON.stringify(defaults));
}


var streams = [];
var messagesDict = {};
var clientArray = [];
var ipAddressCounter = {};
const dgram = require('dgram');
var tcpserver = net.createServer(function (connection) { 
  new AISClient(connection);
  //console.log("num clients: " + clientArray.length);
});
tcpserver.listen(80, function () {
  log('server bound');
});

function log(text) {
  console.log((new Date()).toLocaleString() + ": " + text);
}

function err(text) {
  console.log((new Date()).toLocaleString() + ": " + text);
}



class AISClient {
  constructor(connection) {
    /*console.log('-----------');
    console.log(ipAddressCounter);
    console.log('-----------');*/
    var self = this;
    this.id = clientId++;
    this.connection = connection;
    this.ip = this.connection.remoteAddress;
    
    if (!ipAddressCounter[this.ip]) {
      ipAddressCounter[this.ip] = 0;
    }

    if (ipAddressCounter[this.ip] > 3) {
      //console.log('number of connections from ' + this.ip + '(' + ipAddressCounter[this.ip] + ') exceeds maximum.');
      this.connection.end();
      return;
    }
    
    ipAddressCounter[this.ip] += 1;
    clientArray.push(this);
    this.connection.write("# Exploratorium.edu San Francisco Bay Area AIS feed\n");
    this.connection.write("# Data provided free for non-commercial use only\n");
    this.connection.write("# Best efforts service, no liability accepted for outages or errors\n");
    
    request('http://freegeoip.net/json/' + this.ip, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        self.geostats = JSON.parse(body);
        //console.log(self.geostats);
        self.geostats.id = self.id;
        io.emit('client_joined', self.geostats);
      }
      else {
      }
    });   
    //console.log("new connection: " + this.ip);
    this.connection.on('error', function (exc) {
      log("ignoring exception: ", exc);
      self.remove();
    });
    
    this.connection.on('end', function () {
      //console.log("disconnected client " + self.ip);
      self.remove();
      //console.log("num clients: " + clientArray.length);
    });
  }
  
  write(str) {
    try {
      this.connection.write(str);
    }
    catch (err) {
      log("writing failed. removing " + this.ip);
      this.remove();
    }
  }
  remove() {
    ipAddressCounter[this.ip] -= 1;
    io.emit('client_left', this.geostats);
    clientArray.splice(clientArray.indexOf(this), 1);
  }
}
class AISStream {
  constructor(_params) {
    this.params = _params;
    this.connected = false;
    this._lastDataTime = null;
    this.connect();
  }
  connect() {
    this.connected = false;
    this.connectStream();
  }
  set connected(c) {
    io.emit('status', {
      name: this.params.name, 
      online: c
    });
    this._connected = c;
    console.log(this.params.name + " connected: " + c);
  }
  get connected() {
    return this._connected;
  }
  
  get lastDataTime() {
    return this._lastDataTime;
  } 
  decode(str) {
    this._lastDataTime = new Date();
    var payload = (str.split(','))[5];
    var decoded = aisDecoder.decode(str);
    if (messagesDict.hasOwnProperty(payload)) {
      io.emit('duplicate', {
        name: this.params.name,
        data: str, 
        decoded: decoded
      });
    }
    else {
      messagesDict[payload] = new Date();
      io.emit('client_data', {
        name: this.params.name,
        data: str, 
        decoded: decoded, 
        lastDataTime: this.lastDataTime
      });
      var clientIndex = clientArray.length - 1;
      while (clientIndex > 0) {
        clientArray[clientIndex--].write(str);
      }
    }
  }
}
class TCPStream extends AISStream {
  constructor(_params) {
    super(_params);
    var self = this;
    setInterval(function() {
      console.log("test connectivity for " + self.params.name + ": " + self.connected);
      if (!self.connected) {
        console.log("idle too long. trying to reconnect");
        try {
          self.socket.destroy();
          self.connectStream();
        }
        catch(err) {
          console.error(err);
        }
      } else {
        console.log("alive and well");
        self.connected = false;
      }
    }, 1000 * 60 * 5); // 5 minutes
  }
  
  connectStream() {
    this.socket = new net.Socket();
    var self = this;
    var str = "";
    log("connecting to:", this.params);
    this.socket.connect(this.params.port, this.params.url, function () {
      self.connected = true;
    });
    this.socket.on('error', this.reconnect);
    this.socket.on('end', this.reconnect);
    this.socket.on('close', this.reconnect);
    this.socket.on('data', function (data) {
      self.connected = true;
      for (var i = 0; i < data.length; i++) {
        str += String.fromCharCode(data[i]);
        if (data[i] == 10) {
          self.decode(str);
          str = "";
        }
      }
    });
  }
  reconnect(err) {
    err("ERROR: connection to " + self.params.url + " failed: ", err);
    setTimeout(function () {
      this.connect();
    }, 300000);
  }
}
class UDPStream extends AISStream {
  constructor(_params) {
    super(_params);
  }
  connectStream() {
    var self = this;
    this.udpserver = dgram.createSocket('udp4');
    this.udpserver.on('error', (err) => {
      err('server error:\n${err.stack}');
      self.udpserver.close();
      setTimeout(function () {
        self.connect();
      }, 10000);
    });
    this.udpserver.on('message', (msg, rinfo) => {
      //console.log(['server got:', msg.toString(), rinfo.address, rinfo.port]);
      this.decode(msg.toString());
    });
    this.udpserver.on('listening', () => {
      var address = self.udpserver.address();
      log(['server listening', address.address, address.port]);
    });
    log(this.params);
    this.udpserver.bind(this.params.port);
    this.connected = true;
  }
}
streamsJson.forEach(function (s) {
  switch (s.type) {
  case "tcp":
    streams.push(new TCPStream(s));
    break;
  case "udp":
    streams.push(new UDPStream(s));
    break;
  }
});
io.on('connection', function (socket) {
  socket.on('event', function (data) {});
  socket.on('disconnect', function () {});
  socket.on('ready', function (data) {
    console.log("dashboard ready");
    console.log(streams.length);
    streams.forEach(function (s) {
      log('status', {
        name: s.params.name
        , online: s.connected
      });
      io.emit('status', {
        name: s.params.name, 
        online: s.connected
      });
      io.emit('last_data_time', {
        name: s.params.name, 
        lastDataTime: s.lastDataTime
      });
    });
    clientArray.forEach(function (c) {
      io.emit('client_joined', c.geostats);
    });
  });
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(express.static(path.join(__dirname, 'public')));
server.listen(3000);
app.get('/', function (req, res) {
  res.render('index.html');
});
app.get('/streams', function (req, res) {
  res.send(streamsJson);
});

setInterval(function () {
  var now = new Date();
  for (var m in messagesDict) {
    if (now - messagesDict[m] > 10000) {
      delete messagesDict[m];
    }
  }
}, 10000);
module.exports = app;
