var app = angular.module('aisagg', []);

app.controller('all_streams_controller', function($scope, $http) {
  
  $scope.streamList = [];
  $http.get("streams")
    .then(function(response) {
        $scope.streamList = response.data;
        console.log($scope.streamList);
    });
});

var socket = io('http://localhost:3000');
o